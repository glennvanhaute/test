@extends('app')



@section('content')
<div class="container">


    <div id="pageheader">
        <h1>Todo list</h1>
    </div>


    <div id="homepage" class="container-fluid">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    Add item <br>
                    <form id="todo" action="/todo" class="form" method="POST">


                        {!! csrf_field() !!}
                        <div class="group">
                            <input type="text" placeholder="" name="todo" value="" required>
                        </div>

                        <button class="btnLogin btn btn-success" type="submit"><span>Add</span></button>

                    </form>
                </div>
            </div>
           <div class="row">
               <div class="col-lg-12">
                   <h2>Not Done</h2>
                   @foreach($tasks as $task)

                       <h3>{{$task->text}} </h3><br>
                   @endforeach
               </div>
           </div>

            <div class="row">
               <div class="col-lg-12">
                   <h2>Done</h2>
               </div>
           </div>
        </div>
    </div>
</div>
@endsection

