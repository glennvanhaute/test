<?php

namespace App\Http\Controllers;

use Auth;

use App\Http\Requests;
use App\todo;
use App\Http\Requests\todorequest;
use Input;
use DB;
use Carbon;
class todoController extends Controller
{


    public function add (todorequest $request){
        $query = $request->get("todo");

        $todo = new todo();
        $todo->text = $query;
        $todo->save();

        $tasks = DB::table('todo')->get();

        return view('todo',compact('tasks'));
    }
    public function show (){
        $tasks = DB::table('todo')->get();
        return view('todo', compact('tasks'));
    }




}

